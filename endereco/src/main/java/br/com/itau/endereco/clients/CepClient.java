package br.com.itau.endereco.clients;

import br.com.itau.endereco.models.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "viacep", url = "https://viacep.com.br/ws/", configuration = CepClientConfiguration.class)
public interface CepClient {

    @GetMapping("/{cep}/json/")
    @NewSpan(name = "viacep-client")
    Cep consultarCep(@SpanTag("cep") @PathVariable String cep);
}
