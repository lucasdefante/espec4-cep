package br.com.itau.endereco.controller;

import br.com.itau.endereco.clients.CepClient;
import br.com.itau.endereco.models.Cep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    @Autowired
    private CepClient cepClient;

    @PostMapping("/{numCep}")
    public Cep buscaCep(@PathVariable String numCep) {
        return cepClient.consultarCep(numCep);
    }
}
