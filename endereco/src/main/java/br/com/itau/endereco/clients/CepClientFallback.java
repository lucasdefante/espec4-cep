package br.com.itau.endereco.clients;

import br.com.itau.endereco.models.Cep;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CepClientFallback implements CepClient {

    @Override
    public Cep consultarCep(String cep) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço VIACEP indisponível no momento. Tente novamente mais tarde.");
    }
}
