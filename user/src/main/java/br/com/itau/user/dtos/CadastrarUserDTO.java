package br.com.itau.user.dtos;

import javax.validation.constraints.NotNull;

public class CadastrarUserDTO {

    @NotNull
    private String nome;

    @NotNull
    private String cep;

    public CadastrarUserDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }
}
