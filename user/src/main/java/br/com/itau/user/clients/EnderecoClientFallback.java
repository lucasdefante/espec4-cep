package br.com.itau.user.clients;

import br.com.itau.user.models.Cep;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EnderecoClientFallback implements EnderecoClient {

    @Override
    public Cep buscarEndereco(String cep) {
        throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Serviço ENDERECO indisponível no momento. Tente novamente mais tarde.");
    }
}
