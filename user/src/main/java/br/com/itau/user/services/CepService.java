package br.com.itau.user.services;

import br.com.itau.user.models.Cep;
import br.com.itau.user.repositories.CepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CepService {

    @Autowired
    private CepRepository cepRepository;

    public Cep salvarCep(Cep cep) {
        return cepRepository.save(cep);
    }
}
