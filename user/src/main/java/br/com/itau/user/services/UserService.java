package br.com.itau.user.services;

import br.com.itau.user.exceptions.UserNotFoundException;
import br.com.itau.user.models.Cep;
import br.com.itau.user.models.User;
import br.com.itau.user.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CepService cepService;

    public User cadastrarUser(User user) {
        Cep endereco = cepService.salvarCep(user.getEndereco());
        user.setEndereco(endereco);
        return userRepository.save(user);
    }

    public User exibirUser(int id) {
        Optional<User> optional = userRepository.findById(id);
        if(!optional.isPresent()) {
            throw new UserNotFoundException();
        }
        return optional.get();
    }
}
