package br.com.itau.user.clients;

import br.com.itau.user.models.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "endereco", configuration = EnderecoClientConfiguration.class)
public interface EnderecoClient {

    @PostMapping("/endereco/{cep}")
    @NewSpan(name = "endereco-client")
    Cep buscarEndereco(@SpanTag("cep") @PathVariable String cep);
}
