package br.com.itau.user.dtos;

import br.com.itau.user.models.Cep;
import br.com.itau.user.models.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User toUser(CadastrarUserDTO cadastrarUserDTO, Cep endereco) {
        User user = new User();
        user.setNome(cadastrarUserDTO.getNome());
        user.setEndereco(endereco);
        return user;
    }
}
