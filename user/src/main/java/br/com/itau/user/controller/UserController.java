package br.com.itau.user.controller;

import br.com.itau.user.clients.EnderecoClient;
import br.com.itau.user.dtos.CadastrarUserDTO;
import br.com.itau.user.dtos.UserMapper;
import br.com.itau.user.models.Cep;
import br.com.itau.user.models.User;
import br.com.itau.user.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private EnderecoClient enderecoClient;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @PostMapping
    public User cadastrarUser(@RequestBody @Valid CadastrarUserDTO cadastrarUserDTO) {
        Cep cep = enderecoClient.buscarEndereco(cadastrarUserDTO.getCep());
        User user = userMapper.toUser(cadastrarUserDTO, cep);
        return userService.cadastrarUser(user);
    }

    @GetMapping("/{id}")
    public User exibirUser(@PathVariable int id) {
        return userService.exibirUser(id);
    }
}
