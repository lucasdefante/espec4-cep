package br.com.itau.user.repositories;

import br.com.itau.user.models.Cep;
import org.springframework.data.repository.CrudRepository;

public interface CepRepository extends CrudRepository<Cep, Integer> {
}
